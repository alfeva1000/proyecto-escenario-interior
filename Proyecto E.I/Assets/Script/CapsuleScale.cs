using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleScale : MonoBehaviour
{
    [SerializeField]
    private Vector3 axes;       // Los posibles ejes de Esacalado
    public float scaleUnits;    // La velocidad de Escalado
   
    // Update is called once per frame
    void Update()
    {
        //Acotación de los valores de escalado al valor unitario [-1,1]
        axes = CapsuleMovement.ClampVector3(axes);

        // La escala, al contrario que la rotacion y el movimiento, es acumulativa
        // Lo que quiere decir que debemos añadir el nuevo valor de la escala, al valor anterior.
        transform.localScale += axes * (scaleUnits * Time.deltaTime);
    }
}
